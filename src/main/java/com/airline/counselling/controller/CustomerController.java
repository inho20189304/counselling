package com.airline.counselling.controller;

import com.airline.counselling.model.CustomerRequest;
import com.airline.counselling.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.apache.coyote.Request;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody CustomerRequest request) {
        customerService.setCustomer(request.getName(), request.getPhone());

        return "ok";

    }
}

package com.airline.counselling.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;


@Entity // 테이블 생성 위함
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // 규칙 = 1씩 더함
    private Long id;
    @Column(nullable = false, length = 20) // 값이 꼭 들어가야해서 nullalbe = false 사용
    private String name;
    @Column(nullable = false, length = 20) // 값이 꼭 들어가야해서 nullalbe = false 사용
    private String phone;

}

package com.airline.counselling.service;

import com.airline.counselling.entity.Customer;
import com.airline.counselling.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer (String name, String phone) {
        Customer adddata = new Customer();
        adddata.setName(name);
        adddata.setPhone(phone);

        customerRepository.save(adddata);

    }
}

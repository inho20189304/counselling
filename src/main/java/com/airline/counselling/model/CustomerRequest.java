package com.airline.counselling.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerRequest {
    private String name;

     private String phone;
}
